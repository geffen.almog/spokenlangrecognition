import librosa
import numpy as np
import warnings
from glob import glob
import json
from pydub import AudioSegment

DATASET_PATH = "audio\\"
JSON_PATH = "data.json"
SAMPLE_RATE = 44100
labels = {'English': 1, 'French': 2}
languages = ['English', 'French']
audio_files_train_and_test = {}

image_width = 500
image_height = 128

data = {
    "labels": [],
    "spectogram": []
}

def convert_any_file_to_wav(file_path):
    """
    this function get a path to audio file and if is nessery it
    wiil change the audio file to type of wav
    :param file_path: file to
    :return: path to audio of wav
    """
    try:
        load_audio_file(file_path)
        if file_path.endswith(".mp3"):
            file_path = convert_mp3_to_wav(file_path)
        return file_path

    except Exception as ex:
            file_path = convert_mp4_to_wav(file_path)
            return file_path


def convert_mp3_to_wav(file_path):
    """
    this function will change the type of the file from mp3 to wav
    :param file_path: path to file of mp3
    :return: path of the wav file
    """
    sound = AudioSegment.from_mp3(file_path)
    sound.export('newfile.wav', format="wav")
    return 'newfile.wav'

def convert_mp4_to_wav(file_path):
    """
     this function will change the type of the file from mp4 to wav
    :param file_path: path to file of mp4
    :return: path to the file of wav
    """
    sound = AudioSegment.from_file(file_path, format='mp4')
    sound.export(f'newfile.wav', format="wav")
    return 'newfile.wav'

def load_audio_file(audio_file_path):
    """
    this function load the audio file
    :param audio_file_path: audio file path to load
    :return: audio_segment, sample_rate
    """
    warnings.simplefilter('ignore', UserWarning)
    audio_segment, sample_rate = librosa.load(audio_file_path)
    return audio_segment, sample_rate

def spectrogram(audio_segment):
    """
    this function take audio segment and to a spectogram and normalize and scale
    :param audio_segment: audio segment to do a spectrogram
    :return: spectrogram to
    """
    spec = librosa.feature.melspectrogram(audio_segment, n_mels=image_height, hop_length=512)
    image = librosa.core.power_to_db(spec)
    image_np = np.asmatrix(image)

    # Normalize and scale
    image_np_scaled_temp = (image_np - np.min(image_np))
    image_np_scaled = image_np_scaled_temp / np.max(image_np_scaled_temp)

    return image_np_scaled


def to_integer(image_float):
    """
    this function get a picturs in array of float number in range (0,1)
    and convert the array to integer number in range of (0,255)
    :param image_float: array of spectogrem in float number
    :return: the array in unit8 - numbers between 0 - 255
    """
    return (image_float * 255.).astype(np.uint8)


def make_db():
    """
    the function make a db that will fit
    to the model
    :return: none
    """
    audio_files = {}
    print(1)
    #make the db in a dict
    for lang in languages:
        audio_files[lang] = glob(DATASET_PATH + lang + '\\' + '\\*.wav')
        for f in audio_files[lang]:
            audio_segment, sample_rate = load_audio_file(f)
            data["spectogram"].append(to_integer(spectrogram(audio_segment)).tolist())
            data["labels"].append(labels[lang])

    #save the db in json file
    with open(JSON_PATH, "w") as fp:
        json.dump(data, fp, indent=4)



if __name__ == "__main__":
    make_db()


