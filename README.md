# Spoken Language Recognition


## About The Project...

This is a machine learning project written in Python.
The purpose of the project is to get an audio file,
and identify what language was spoken in the audio file.


## in order to run the project you need to have on your cp...

- [ ] [FFMPEG](https://ffmpeg.org/)
- [ ] python 64 bit version 3.7.0 

also, you need to have all of the files in the same folder.